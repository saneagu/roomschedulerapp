package ro.orangeStartIT;

import java.util.ArrayList;
public class Room {
    private String name;
    private int capacity;
    boolean projector;

    public static ArrayList<Room> rooms = new ArrayList<>();
    //public static ArrayList<String> roomNames = new ArrayList<>();

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}