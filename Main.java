package ro.orangeStartIT;
import java.io.*;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
public class Main {

    public static void main(String[] args) {
        int day, year, month, hour;
        Scanner sc = new Scanner(System.in);
        Calendar cal = Calendar.getInstance();
        List<Appointments> dbAppoinments = new ArrayList<>();

        // Read date from keyboard
        System.out.println("Select Date for reservation: ");
        System.out.print("Day: ");
        day = sc.nextInt();
        System.out.print("Month: ");
        month = sc.nextInt();
        System.out.print("Year: ");
        year = sc.nextInt();
        System.out.print("Hour: ");
        hour = sc.nextInt();

        cal.set(year, month - 1, day, hour, 0, 0);
        long timestamp = cal.getTimeInMillis() / 1000;

       new OperateFile().writeAppt(timestamp);


    }
}
