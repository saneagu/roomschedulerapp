package ro.orangeStartIT;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class OperateFile {
    String pathBaza = "C:\\Orange\\OrangeTraining\\RSA\\bazaRezervari.csv";
    public static String roomsPath = "C:\\Orange\\OrangeTraining\\RSA\\rooms.txt";
    File fisier = new File(pathBaza);
    BufferedWriter bw;

    {
        try {
            bw = new BufferedWriter(new FileWriter(fisier, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Appointments appo = new Appointments();
    long input;

    public static boolean isAvailable(long timestampSearch, ArrayList<Appointments> appts) {
        boolean ret = true;
        for (Appointments item : appts) {
            if (item.getTimestamp() == timestampSearch) {
                ret = false;
                break;
            }
        }
        if (ret) {
           // System.out.println("Free room!");
        } else System.out.println("Sorry, occupied!");
        return ret;
    }

    public ArrayList<Appointments> readDB() {
        ArrayList<Appointments> appo = new ArrayList<>();
        try {
            File file = new File(pathBaza);

            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                String[] parts = sc.nextLine().split(",");
                long ts = Long.parseLong(parts[1]);
                appo.add(new Appointments(new Room(parts[0]), ts));

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return appo;
    }

    public static ArrayList<Room> readRoomsfromFile() {
        ArrayList<Room> initialRooms = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(roomsPath));
            String nameFromFile;
            while ((nameFromFile = br.readLine()) != null) {
                initialRooms.add(new Room(nameFromFile));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Exceptie fisierul nu exista");
        } catch (IOException e) {
            System.out.println("alta eraore i/o");
        }
        return initialRooms;
    }


    public void writeAppt(long input) {
        ArrayList<Appointments> appo = readDB();
        ArrayList<Room> rooms = readRoomsfromFile();

        if (isAvailable(input, appo)) {
            System.out.println("Free rooms");
            Appointments.writeAppointment(rooms.get(0), input, bw);
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else System.out.println("Busy!");
    }
}
