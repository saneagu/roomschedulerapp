package ro.orangeStartIT;


import java.io.BufferedWriter;
import java.io.FileNotFoundException;

import java.io.IOException;

public class Appointments {

    Room room;
    long timestamp;

    public Appointments(Room room, long ts) {
        this.room = room;
        this. timestamp = ts;
    }

    public Appointments() {

    }


    public Room getRoom() {
        return room;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public static void writeAppointment(Room room, long timestamp, BufferedWriter br){

        try {

            br.write(room.getName() + "," + timestamp);
            br.newLine();

        } catch (FileNotFoundException e) {
            System.out.println("Fisierul nu exista!");
        } catch (IOException e) {
            System.out.println("Alta exceptie IO!");
        }

    }

}